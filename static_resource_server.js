/* Created by instancetype on 8/7/14. */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  http = require('http')
, fs = require('fs')

function serveStaticFile(res, path, contentType, responseCode) {
  responseCode = responseCode || 200

  fs.readFile(__dirname + path, function(err, data) {
    if (err) {
      res.writeHead(500, { 'Content-Type' : 'text/plain' })
      res.end('500 - Internal Error')
    }
    else {
      res.writeHead(responseCode, { 'Content-Type' : contentType })
      res.end(data)
    }
  })
}

http.createServer(function(req, res) {
  'use strict'

  let path = req.url.replace(/\/?(?:\?.*)?$/,'').toLowerCase()
  switch (path) {
    case '':
      serveStaticFile(res, '/public/home.html', 'text/html')
      break

    case '/about':
      serveStaticFile(res, '/public/about.html', 'text/html')
      break

    case '/img/logo.png':
      serveStaticFile(res, '/public/img/logo.png', 'image/png')
      break

    default :
      serveStaticFile(res, '/public/404.html', 'text/html', 404)
      break
      break
  }
}).listen(3000, function() {
  console.log('Listening on port 3000...')
})