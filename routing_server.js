/* Created by instancetype on 8/7/14. */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const http = require('http')

http.createServer(function(req, res) {
  'use strict'

  let path = req.url.replace(/\/?(?:\?.*)?$/,'').toLowerCase()
  switch (path) {
    case '':
      res.writeHead(200, { 'Content-Type' : 'text/plain' })
      res.end('Home')
      break

    case '/about':
      res.writeHead(200, { 'Content-Type' : 'text/plain' })
      res.end('About')
      break

    default :
      res.writeHead(404, { 'Content-Type' : 'text/plain' })
      res.end('Not Found')
      break
  }
}).listen(3000, function() {
  console.log('Listening on port 3000...')
})